# **Escapy2.0** 
[![Build Status](http://174.138.0.194:1997/buildStatus/icon?job=tinder-samurai/escapy2.0/release)](http://174.138.0.194:1997/job/tinder-samurai/job/escapy2.0/job/release/) [![Sonarcloud](https://sonarcloud.io/api/project_badges/measure?project=escapy-2&metric=alert_status)](https://sonarcloud.io/dashboard?id=escapy-2)
### Java 2D game engine (and game project)

 * [**Engine Documentation and User Guide (old)**](https://bitbucket.org/tinder-samurai/escapy2.0/src/master/engine/doc/tex/Escapy2Doc.pdf)
 * [**Game Design document**](https://bitbucket.org/tinder-samurai/escapy-des-doc/src/master/)
 * [**EscapyActiveComponent**](https://bitbucket.org/tinder-samurai/escapy2.0/src/master/libs/src/main/java/net/tindersamurai/activecomponent/)
 * [**EscapyGdx (old)**](https://bitbucket.org/tinder-samurai/escapy/src/master/)
 * [**GrInjector**](https://bitbucket.org/tinder-samurai/grinjector/src/master/)